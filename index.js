const express = require('express');
const app = express();
const port = 3000;

app.use(express.json());
app.use(express.urlencoded({extended: true}));

// Mock Data Base
let usersDb = [
    {
        "username": "johndoe",
        "password": "johndoe1234"
    }
];
// End of Mock Data Base


// /home
app.get('/home', (request, response) => {
    response.send('Welcome to Home Page')
});

// /users
app.get('/users', (request, response) => {
    response.send(usersDb);

    // Check the usersDb
    console.log(usersDb);
})

// /delete-user
app.delete('/delete-user', (request, response) => {

    let message = '';

    console.log('This is the current db: ' + usersDb);
    console.log('This is the current length: ' + usersDb.length);

    if (usersDb.length == 0) {
        message = 'Users database is empty. Nothing to delete.';
    } 

    else if (usersDb.length > 0 && request.body.username == '') {
        message = 'Please input the username to be deleted';
    }

    else {

        let indexToDelete;
    
        for (let i = 0; i < usersDb.length; i++) {

            // Find the index of the user to be deleted...
            if (usersDb[i].username == request.body.username) {
                indexToDelete = i;
                message = `User ${usersDb[i].username} has been deleted`;

                // remove the user
                 usersDb.splice(indexToDelete, 1);

                break;
            }
        }

        if (indexToDelete == undefined) {
            message = `User "${request.body.username} does not exist."`;
        }
    }

    response.send(message);
    console.log(usersDb);

});

app.listen(port, () => console.log(`Server is running at port ${port}`));